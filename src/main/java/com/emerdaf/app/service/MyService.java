/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emerdaf.app.service;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.domain.security.UserEntity;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

/**
 *
 * @author Clémence
 */
@Singleton
public class MyService {

    @PersistenceContext(unitName = "persistenceUnit")
    private EntityManager em;
  
    public void persist(Object object) {
        
            em.persist(object);
     
    }

    public void addCompte(CompteEntity compte, UserEntity user) {
        compte.updateAuditInformation(user.getUsername());
        persist(compte);
    }
}
