package com.emerdaf.app.service;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.domain.CompteStatus;
import com.emerdaf.app.domain.NoeudEntity;
import com.emerdaf.app.domain.security.UserEntity;
import com.emerdaf.app.service.security.SecurityWrapper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;

@Named
public class CompteService extends BaseService<CompteEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    public CompteService() {
        super(CompteEntity.class);
    }

    @Transactional
    public List<CompteEntity> findAllCompteEntities() {
        return entityManager.createQuery("SELECT o FROM Compte o ", CompteEntity.class).getResultList();
    }

    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Compte o", Long.class).getSingleResult();
    }

    @Override
    @Transactional
    public CompteEntity save(CompteEntity compte) {
        String username = SecurityWrapper.getUsername();

        compte.updateAuditInformation(username);

        return super.save(compte);
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public CompteEntity add(CompteEntity compte, UserEntity user) {
        compte.updateAuditInformation(user.getUsername());
        return super.save(compte);
    }

    @Override
    @Transactional
    public CompteEntity update(CompteEntity compte) {
        String username = SecurityWrapper.getUsername();
        compte.updateAuditInformation(username);
        return super.update(compte);
    }

    @Override
    protected void handleDependenciesBeforeDelete(CompteEntity compte) {

        /* This is called before a Compte is deleted. Place here all the
         steps to cut dependencies to other entities */
        this.cutAllCompteEnfantNoeudsAssignments(compte);
        this.cutAllCompteParentNoeud2sAssignments(compte);

    }

    // Remove all assignments from all noeud a compte. Called before delete a compte.
    @Transactional
    private void cutAllCompteEnfantNoeudsAssignments(CompteEntity compte) {
        entityManager
                .createQuery("UPDATE Noeud c SET c.compteEnfant = NULL WHERE c.compteEnfant = :p")
                .setParameter("p", compte).executeUpdate();
    }

    // Remove all assignments from all noeud2 a compte. Called before delete a compte.
    @Transactional
    private void cutAllCompteParentNoeud2sAssignments(CompteEntity compte) {
        entityManager
                .createQuery("UPDATE Noeud c SET c.compteParent = NULL WHERE c.compteParent = :p")
                .setParameter("p", compte).executeUpdate();
    }

    @Transactional
    public List<CompteEntity> findAvailableComptes(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Compte o WHERE o.client IS NULL", CompteEntity.class).getResultList();
    }

    @Transactional
    public List<CompteEntity> findComptesByClient(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Compte o WHERE o.client = :user", CompteEntity.class).setParameter("user", user).getResultList();
    }

    // Find all noeud which are not yet assigned to a compte
    @Transactional
    public List<CompteEntity> findAvailableCompteEnfant(NoeudEntity noeud) {
        Long id = -1L;
        if (noeud != null && noeud.getCompteEnfant() != null && noeud.getCompteEnfant().getId() != null) {
            id = noeud.getCompteEnfant().getId();
        }
        return entityManager.createQuery(
                "SELECT o FROM Compte o where o.id NOT IN (SELECT p.compteEnfant.id FROM Noeud p where p.compteEnfant.id != :id)", CompteEntity.class)
                .setParameter("id", id).getResultList();
    }

    // Find all noeud2 which are not yet assigned to a compte
    @Transactional
    public List<CompteEntity> findAvailableCompteParent(NoeudEntity noeud2) {
        Long id = -1L;
        if (noeud2 != null && noeud2.getCompteParent() != null && noeud2.getCompteParent().getId() != null) {
            id = noeud2.getCompteParent().getId();
        }
        return entityManager.createQuery(
                "SELECT o FROM Compte o where o.id NOT IN (SELECT p.compteParent.id FROM Noeud p where p.compteParent.id != :id)", CompteEntity.class)
                .setParameter("id", id).getResultList();
    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<CompteEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Compte o");

        // Can be optimized: We need this join only when client filter is set
        query.append(" LEFT OUTER JOIN o.client client");

        String nextConnective = " WHERE";

        Map<String, Object> queryParameters = new HashMap<>();

        if (filters != null && !filters.isEmpty()) {

            nextConnective += " ( ";

            for (String filterProperty : filters.keySet()) {

                if (filters.get(filterProperty) == null) {
                    continue;
                }

                switch (filterProperty) {

                    case "categorie":
                        query.append(nextConnective).append(" o.categorie = :categorie");
                        queryParameters.put("categorie", new Integer(filters.get(filterProperty).toString()));
                        break;

                    case "status":
                        query.append(nextConnective).append(" o.status = :status");
                        queryParameters.put("status", CompteStatus.valueOf(filters.get(filterProperty).toString()));
                        break;

                    case "client":
                        query.append(nextConnective).append(" o.client = :client");
                        queryParameters.put("client", filters.get(filterProperty));
                        break;

                }

                nextConnective = " AND";
            }

            query.append(" ) ");
            nextConnective = " AND";
        }

        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }

        TypedQuery<CompteEntity> q = this.entityManager.createQuery(query.toString(), this.getType());

        for (String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

    public List<CompteEntity> compteBase() {
        return entityManager.createQuery("SELECT o FROM Compte o WHERE o.type= 'EMERDAF'", CompteEntity.class).getResultList();
    }

    public List<CompteEntity> comptesParentsElligibles(CompteEntity c) {
        TypedQuery q = entityManager.createQuery("SELECT o.compteParent AS cmpt , COUNT(o.compteEnfant) AS x FROM Noeud o GROUP BY cmpt", CompteEntity.class);
        List<Object[]> l = q.getResultList();
        String r = "(";
        for (Object[] objects : l) {
            Integer nbre = Integer.valueOf(objects[1].toString());
            if (nbre > 2 && objects[0] != null) {
                r += objects[0].toString() + ",";
            }
        }
        r += "" + c.getId() + ")";

        List<CompteEntity> resultList = entityManager.createQuery("SELECT o FROM Compte o WHERE o.categorie= :cat AND o.status = :status AND o.id NOT IN " + r, CompteEntity.class)
                .setParameter("cat", c.getCategorie())
                .setParameter("status", CompteStatus.PAYE).getResultList();
        return resultList;
    }

    public List<CompteEntity> comptesParentsElligibles(NoeudEntity noeud) {
        CompteEntity c = noeud.getCompteEnfant();
        TypedQuery q = entityManager.createQuery("SELECT o.compteParent AS cmpt , COUNT(o.compteEnfant) AS x FROM Noeud o GROUP BY cmpt", CompteEntity.class);
        List<Object[]> l = q.getResultList();
        String r = "(";
        for (Object[] objects : l) {
            Integer nbre = Integer.valueOf(objects[1].toString());
            if (nbre > 2 && objects[0] != null) {
                r += objects[0].toString() + ",";
            }
        }
        r += "" + c.getId() + ")";

        List<CompteEntity> resultList = entityManager.createQuery("SELECT o FROM Compte o WHERE o.categorie= :cat AND o.status = :status AND o.id NOT IN " + r, CompteEntity.class)
                .setParameter("cat", c.getCategorie())
                .setParameter("status", CompteStatus.PAYE).getResultList();
        return resultList;
    }

    public CompteEntity getParent(CompteEntity c) {
        try {
            return entityManager.createQuery("SELECT o.compteParent FROM Noeud o WHERE o.compteEnfant= :cmpt", CompteEntity.class)
                    .setParameter("cmpt", c)
                    .getSingleResult();
        } catch (NoResultException e) {
            return new CompteEntity();
        }

    }

    public List<CompteEntity> getCmptEnfants(CompteEntity c) {
        return entityManager.createQuery("SELECT o.compteEnfant FROM Noeud o WHERE o.compteParent= :cmpt", CompteEntity.class)
                .setParameter("cmpt", c)
                .getResultList();
    }

}
