package com.emerdaf.app.service.security;

import com.emerdaf.app.domain.security.RolePermission;
import com.emerdaf.app.domain.security.UserRole;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

//@Singleton
//@Startup
public class RolePermissionsPublisher {

    private static final Logger logger = Logger.getLogger(RolePermissionsPublisher.class.getName());
    
    @Inject
    private RolePermissionsService rolePermissionService;
    
    @PostConstruct
    public void postConstruct() {

        if (rolePermissionService.countAllEntries() == 0) {

            rolePermissionService.save(new RolePermission(UserRole.Administrator, "compte:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "compte:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "compte:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "compte:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.Registered, "compte:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Registered, "compte:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Registered, "compte:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "noeud:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "noeud:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "noeud:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "noeud:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.Registered, "noeud:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "user:*"));
            
            logger.info("Successfully created permissions for user roles.");
        }
    }
}
