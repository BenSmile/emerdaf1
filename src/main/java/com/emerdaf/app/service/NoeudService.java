package com.emerdaf.app.service;

import com.emerdaf.app.domain.NoeudEntity;
import com.emerdaf.app.service.security.SecurityWrapper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;

@Named
public class NoeudService extends BaseService<NoeudEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public NoeudService(){
        super(NoeudEntity.class);
    }
    
    @Transactional
    public List<NoeudEntity> findAllNoeudEntities() {
        
        return entityManager.createQuery("SELECT o FROM Noeud o ", NoeudEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Noeud o", Long.class).getSingleResult();
    }
    
    @Override
    @Transactional
    public NoeudEntity save(NoeudEntity noeud) {
        String username = SecurityWrapper.getUsername();
        
        noeud.updateAuditInformation(username);
        
        return super.save(noeud);
    }
    
    @Override
    @Transactional
    public NoeudEntity update(NoeudEntity noeud) {
        String username = SecurityWrapper.getUsername();
        noeud.updateAuditInformation(username);
        return super.update(noeud);
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(NoeudEntity noeud) {

        /* This is called before a Noeud is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<NoeudEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Noeud o");
        
        String nextConnective = " WHERE";
        
        Map<String, Object> queryParameters = new HashMap<>();
        
        if (filters != null && !filters.isEmpty()) {
            
            nextConnective += " ( ";
            
            for(String filterProperty : filters.keySet()) {
                
                if (filters.get(filterProperty) == null) {
                    continue;
                }
                
                switch (filterProperty) {
                
                }
                
                nextConnective = " AND";
            }
            
            query.append(" ) ");
            nextConnective = " AND";
        }
        
        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }
        
        TypedQuery<NoeudEntity> q = this.entityManager.createQuery(query.toString(), this.getType());
        
        for(String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
