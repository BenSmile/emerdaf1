package com.emerdaf.app.domain;

import com.emerdaf.app.domain.security.UserEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name = "Compte")
@Table(name = "\"COMPTE\"")
@XmlRootElement
public class CompteEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "\"categorie\"")
    @Digits(integer = 7, fraction = 0)
    @NotNull
    private Integer categorie;

    @ManyToOne(optional = true)
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID")
    private UserEntity client;

    @Column(name = "\"STATUS\"")
    @Enumerated(EnumType.STRING)
    private CompteStatus status;

    @Size(max = 10)
    @Column(length = 10, name = "\"TYPE\"")
    private String type;

    @Column(name = "ENTRY_CREATED_BY")
    private String createdBy;

    @Column(name = "ENTRY_CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "ENTRY_MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "ENTRY_MODIFIED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;

    public Integer getCategorie() {
        return this.categorie;
    }

    public void setCategorie(Integer categorie) {
        this.categorie = categorie;
    }

    public UserEntity getClient() {
        return this.client;
    }

    public void setClient(UserEntity user) {
        this.client = user;
    }

    public CompteStatus getStatus() {
        return status;
    }

    public void setStatus(CompteStatus status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void updateAuditInformation(String username) {
        if (this.getId() != null) {
            modifiedAt = new Date();
            modifiedBy = username;
        } else {
            createdAt = new Date();
            modifiedAt = createdAt;
            createdBy = username;
            modifiedBy = createdBy;
        }
    }

    @Override
    public String toString() {
        return getId() + "";
    }

}
