package com.emerdaf.app.domain.security;

/**
 * User role
 * */
public enum UserRole {

    Administrator, Registered;
}
