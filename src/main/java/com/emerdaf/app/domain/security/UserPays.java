package com.emerdaf.app.domain.security;

public enum UserPays {

    RDC, BURUNDI, UGANDA, KENYA;
}
