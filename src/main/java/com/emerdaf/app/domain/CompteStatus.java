package com.emerdaf.app.domain;

public enum CompteStatus {
    
    PAYE, NONPAYE;
}
