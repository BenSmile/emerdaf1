package com.emerdaf.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="Noeud")
@Table(name="\"NOEUD\"")
@XmlRootElement
public class NoeudEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne(optional=true, cascade=CascadeType.DETACH)
    @JoinColumn(name="COMPTEENFANT_ID", nullable=true)
    private CompteEntity compteEnfant;

    @OneToOne(optional=true, cascade=CascadeType.DETACH)
    @JoinColumn(name="COMPTEPARENT_ID", nullable=true)
    private CompteEntity compteParent;

    @Column(name = "ENTRY_CREATED_BY")
    private String createdBy;

    @Column(name = "ENTRY_CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "ENTRY_MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "ENTRY_MODIFIED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    
    public CompteEntity getCompteEnfant() {
        return this.compteEnfant;
    }

    public void setCompteEnfant(CompteEntity compteEnfant) {
        this.compteEnfant = compteEnfant;
    }

    public CompteEntity getCompteParent() {
        return this.compteParent;
    }

    public void setCompteParent(CompteEntity compteParent) {
        this.compteParent = compteParent;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }
    
    public void updateAuditInformation(String username) {
        if (this.getId() != null) {
            modifiedAt = new Date();
            modifiedBy = username;
        } else {
            createdAt = new Date();
            modifiedAt = createdAt;
            createdBy = username;
            modifiedBy = createdBy;
        }
    }
    
}
