package com.emerdaf.app.rest;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.service.CompteService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/comptes")
@Named
public class CompteResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private CompteService compteService;
    
    /**
     * Get the complete list of Compte Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /comptes
     * @return List of CompteEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CompteEntity> getAllComptes() {
        return compteService.findAllCompteEntities();
    }
    
    /**
     * Get the number of Compte Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /comptes/count
     * @return Number of CompteEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return compteService.countAllEntries();
    }
    
    /**
     * Get a Compte Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /comptes/3
     * @param id
     * @return A Compte Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CompteEntity getCompteById(@PathParam("id") Long id) {
        return compteService.find(id);
    }
    
    /**
     * Create a Compte Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New CompteEntity (JSON) <br/>
     * Example URL: /comptes
     * @param compte
     * @return A CompteEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CompteEntity addCompte(CompteEntity compte) {
        return compteService.save(compte);
    }
    
    /**
     * Update an existing Compte Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated CompteEntity (JSON) <br/>
     * Example URL: /comptes
     * @param compte
     * @return A CompteEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CompteEntity updateCompte(CompteEntity compte) {
        return compteService.update(compte);
    }
    
    /**
     * Delete an existing Compte Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /comptes/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteCompte(@PathParam("id") Long id) {
        CompteEntity compte = compteService.find(id);
        compteService.delete(compte);
    }
    
}
