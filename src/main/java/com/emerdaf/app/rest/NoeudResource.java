package com.emerdaf.app.rest;

import com.emerdaf.app.domain.NoeudEntity;
import com.emerdaf.app.service.NoeudService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/noeuds")
@Named
public class NoeudResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private NoeudService noeudService;
    
    /**
     * Get the complete list of Noeud Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /noeuds
     * @return List of NoeudEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<NoeudEntity> getAllNoeuds() {
        return noeudService.findAllNoeudEntities();
    }
    
    /**
     * Get the number of Noeud Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /noeuds/count
     * @return Number of NoeudEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return noeudService.countAllEntries();
    }
    
    /**
     * Get a Noeud Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /noeuds/3
     * @param id
     * @return A Noeud Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public NoeudEntity getNoeudById(@PathParam("id") Long id) {
        return noeudService.find(id);
    }
    
    /**
     * Create a Noeud Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New NoeudEntity (JSON) <br/>
     * Example URL: /noeuds
     * @param noeud
     * @return A NoeudEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public NoeudEntity addNoeud(NoeudEntity noeud) {
        return noeudService.save(noeud);
    }
    
    /**
     * Update an existing Noeud Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated NoeudEntity (JSON) <br/>
     * Example URL: /noeuds
     * @param noeud
     * @return A NoeudEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public NoeudEntity updateNoeud(NoeudEntity noeud) {
        return noeudService.update(noeud);
    }
    
    /**
     * Delete an existing Noeud Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /noeuds/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteNoeud(@PathParam("id") Long id) {
        NoeudEntity noeud = noeudService.find(id);
        noeudService.delete(noeud);
    }
    
}
