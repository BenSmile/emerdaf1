package com.emerdaf.app.web;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.domain.NoeudEntity;
import com.emerdaf.app.service.CompteService;
import com.emerdaf.app.service.NoeudService;
import com.emerdaf.app.service.security.SecurityWrapper;
import com.emerdaf.app.web.generic.GenericLazyDataModel;
import com.emerdaf.app.web.util.MessageFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("noeudBean")
@ViewScoped
public class NoeudBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(NoeudBean.class.getName());

    private GenericLazyDataModel<NoeudEntity> lazyModel;

    private NoeudEntity noeud;

    @Inject
    private NoeudService noeudService;

    @Inject
    private CompteService compteService;

    private List<CompteEntity> availableCompteEnfantList;

    private List<CompteEntity> availableCompteParentList;
    private List<CompteEntity> cmptParents = new ArrayList<>();

    public void prepareNewNoeud() {
        reset();
        this.noeud = new NoeudEntity();
        // set any default values now, if you need
        // Example: this.noeud.setAnything("test");
    }

    public GenericLazyDataModel<NoeudEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(noeudService);
        }
        return this.lazyModel;
    }

    public List<CompteEntity> getCmptParents() {
        return compteService.comptesParentsElligibles(noeud);
    }

    public void setCmptParents(List<CompteEntity> cmptParents) {
        this.cmptParents = cmptParents;
    }

    public String persist() {

        if (noeud.getId() == null && !isPermitted("noeud:create")) {
            return "accessDenied";
        } else if (noeud.getId() != null && !isPermitted(noeud, "noeud:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (noeud.getId() != null) {
                noeud = noeudService.update(noeud);
                message = "message_successfully_updated";
            } else {
                noeud = noeudService.save(noeud);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return null;
    }

    public String delete() {

        if (!isPermitted(noeud, "noeud:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            noeudService.delete(noeud);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void reset() {
        noeud = null;

        availableCompteEnfantList = null;

        availableCompteParentList = null;

    }

    // Get a List of all compteEnfant
    public List<CompteEntity> getAvailableCompteEnfant() {
        if (this.availableCompteEnfantList == null) {
            this.availableCompteEnfantList = compteService.findAvailableCompteEnfant(this.noeud);
        }
        return this.availableCompteEnfantList;
    }

    // Update compteEnfant of the current noeud
    public void updateCompteEnfant(CompteEntity compte) {
        this.noeud.setCompteEnfant(compte);
        // Maybe we just created and assigned a new compteEnfant. So reset the availableCompteEnfantList.
        availableCompteEnfantList = null;
    }

    // Get a List of all compteParent
    public List<CompteEntity> getAvailableCompteParent() {
        if (this.availableCompteParentList == null) {
            this.availableCompteParentList = compteService.findAvailableCompteParent(this.noeud);
        }
        return this.availableCompteParentList;
    }

    // Update compteParent of the current noeud
    public void updateCompteParent(CompteEntity compte) {
        this.noeud.setCompteParent(compte);
        // Maybe we just created and assigned a new compteParent. So reset the availableCompteParentList.
        availableCompteParentList = null;
    }

    public NoeudEntity getNoeud() {
        if (this.noeud == null) {
            prepareNewNoeud();
        }
        return this.noeud;
    }

    public void setNoeud(NoeudEntity noeud) {
        this.noeud = noeud;
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(NoeudEntity noeud, String permission) {

        return SecurityWrapper.isPermitted(permission);

    }

}
