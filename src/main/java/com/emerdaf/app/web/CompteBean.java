package com.emerdaf.app.web;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.domain.CompteStatus;
import com.emerdaf.app.domain.NoeudEntity;
import com.emerdaf.app.domain.security.UserEntity;
import com.emerdaf.app.service.CompteService;
import com.emerdaf.app.service.NoeudService;
import com.emerdaf.app.service.security.SecurityWrapper;
import com.emerdaf.app.service.security.UserService;
import com.emerdaf.app.web.generic.GenericLazyDataModel;
import com.emerdaf.app.web.util.MessageFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("compteBean")
@ViewScoped
public class CompteBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(CompteBean.class.getName());

    private GenericLazyDataModel<CompteEntity> lazyModel;

    private CompteEntity compte;

    @Inject
    private CompteService compteService;
    @Inject
    private NoeudService noeudService;

    @Inject
    private ClientBean clientBean;
    @Inject
    private NoeudBean noeudBean;

    private Integer categorie;

    @Inject
    private UserService userService;

    private List<UserEntity> allClientsList;
    private List<CompteEntity> comptesBase = new ArrayList<>();
    private List<CompteEntity> cmptParents = new ArrayList<>();

    public void prepareNewCompte() {
        reset();
        this.compte = new CompteEntity();
        // set any default values now, if you need
        // Example: this.compte.setAnything("test");
    }

    public GenericLazyDataModel<CompteEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(compteService);
        }
        return this.lazyModel;
    }

    public List<CompteEntity> getComptesBase() {
        return compteService.compteBase();
    }

    public void setComptesBase(List<CompteEntity> comptesBase) {
        this.comptesBase = comptesBase;
    }

    public Integer getCategorie() {
        return categorie;
    }

    public void setCategorie(Integer categorie) {
        this.categorie = categorie;
    }

    public List<CompteEntity> getCmptParents() {
        return compteService.comptesParentsElligibles(compte);
    }

    public void setCmptParents(List<CompteEntity> cmptParents) {
        this.cmptParents = cmptParents;
    }

    public String persist() {

        if (compte.getId() == null && !isPermitted("compte:create")) {
            return "accessDenied";
        } else if (compte.getId() != null && !isPermitted(compte, "compte:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (compte.getId() != null) {
                compte = compteService.update(compte);
//                noeudBean.getNoeud().setCompteEnfant(compte);
//                noeudBean.persist();
                message = "message_successfully_updated";
            } else {
                compte = compteService.save(compte);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return null;
    }

    public String delete() {

        if (!isPermitted(compte, "compte:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            compteService.delete(compte);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void reset() {
        compte = null;

        allClientsList = null;

    }

    // Get a List of all client
    public List<UserEntity> getClients() {
        if (this.allClientsList == null) {
            this.allClientsList = userService.findAllUserEntities();
        }
        return this.allClientsList;
    }

    // Update client of the current compte
    public void updateClient(UserEntity user) {
        this.compte.setClient(user);
        // Maybe we just created and assigned a new user. So reset the allClientList.
        allClientsList = null;
    }

    public SelectItem[] getStatusSelectItems() {
        SelectItem[] items = new SelectItem[CompteStatus.values().length];

        int i = 0;
        for (CompteStatus status : CompteStatus.values()) {
            items[i++] = new SelectItem(status, getLabelForStatus(status));
        }
        return items;
    }

    public String getLabelForStatus(CompteStatus value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_compte_status_" + value);
        return label == null ? value.toString() : label;
    }

    public CompteEntity getCompte() {
        if (this.compte == null) {
            prepareNewCompte();
        }
        return this.compte;
    }

    public void setCompte(CompteEntity compte) {
        this.compte = compte;
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(CompteEntity compte, String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public String creerCompteBase() {
        compte.setStatus(CompteStatus.PAYE);
        UserEntity user = clientBean.getClient();
        compte.setClient(user);
        compte.setType("EMERDAF");
        persist();
        for (int i = 0; i < 3; i++) {
            NoeudEntity noeudEntity = new NoeudEntity();
            noeudEntity.setCompteParent(compte);
            noeudService.save(noeudEntity);
        }
        return null;
    }

    public String creerCompte() {
        compte.setStatus(CompteStatus.NONPAYE);
        UserEntity user = clientBean.getClient();
        compte.setClient(user);
        compte.setType("CLIENT");
        persist();
        NoeudEntity noeudEntity = new NoeudEntity();
        noeudEntity.setCompteEnfant(compte);
        noeudService.save(noeudEntity);
        return null;
    }

    public CompteEntity compteParent(CompteEntity c) {
        return compteService.getParent(c);
    }

}
