package com.emerdaf.app.web.security;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.domain.CompteStatus;
import com.emerdaf.app.domain.NoeudEntity;
import com.emerdaf.app.domain.security.UserEntity;
import com.emerdaf.app.domain.security.UserPays;
import com.emerdaf.app.domain.security.UserProvince;
import com.emerdaf.app.domain.security.UserRole;
import com.emerdaf.app.domain.security.UserStatus;
import com.emerdaf.app.service.CompteService;
import com.emerdaf.app.service.MyService;
import com.emerdaf.app.service.NoeudService;
import com.emerdaf.app.service.security.SecurityWrapper;
import com.emerdaf.app.service.security.UserService;
import com.emerdaf.app.web.CompteBean;
import com.emerdaf.app.web.generic.GenericLazyDataModel;
import com.emerdaf.app.web.util.MessageFactory;
import java.io.IOException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import org.primefaces.event.FlowEvent;

import org.primefaces.model.LazyDataModel;

@Named("userBean")
@ViewScoped
public class UserBean implements Serializable {

    @EJB
    private MyService myService;

    private static final Logger logger = Logger.getLogger(UserBean.class.getName());

    private LazyDataModel<UserEntity> lazyModel;

    private static final long serialVersionUID = 1L;

    private UserEntity user;

    private CompteEntity cmpt;
    private boolean skip;
    private Integer categorie;

    private String codeParent;

    @Inject
    private UserService userService;
    @Inject
    private NoeudService noeudService;

    @Inject
    private CompteBean compteBean;

    private List<UserEntity> admin = new ArrayList<>();

    @Inject
    private CompteService compteService;

    public UserEntity getUser() {
        if (user == null) {
            user = new UserEntity();
        }
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Integer getCategorie() {
        return categorie;
    }

    public void setCategorie(Integer categorie) {
        this.categorie = categorie;
    }

    public String getCodeParent() {
        return codeParent;
    }

    public void setCodeParent(String codeParent) {
        this.codeParent = codeParent;
    }

    public void prepareNewUser() {
        this.user = new UserEntity();
        // set any default values now, if you need
    }

    public List<UserEntity> getAdmin() {
        return userService.findAdmin();
    }

    public void setAdmin(List<UserEntity> admin) {
        this.admin = admin;
    }

    public LazyDataModel<UserEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(userService);
        }
        return this.lazyModel;
    }

    public void persist() {

        String message;

        try {

            if (user.getId() != null) {
                user = userService.update(user);
                message = "message_successfully_updated";
            } else {

                // Check if a user with same username already exists
                if (userService.findUserByUsername(user.getUsername()) != null) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "user_username_exists");
                    facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return;
                }
                // Check if a user with same email already exists            
                if (userService.findUserByEmail(user.getEmail()) != null) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "user_email_exists");
                    facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return;
                }

                user = userService.save(user);
                message = "message_successfully_created";
            }

        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message,
                "User");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public String delete() {
        userService.delete(user);
        FacesMessage facesMessage = MessageFactory.getMessage(
                "message_successfully_deleted", "User");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return null;
    }

    public void reset() {
        user = null;

    }

    public SelectItem[] getRolesSelectItems() {
        SelectItem[] items = new SelectItem[UserRole.values().length];

        int i = 0;
        for (UserRole role : UserRole.values()) {
            String label = MessageFactory.getMessageString(
                    "enumeration_label_user_roles_" + role.toString());
            items[i++] = new SelectItem(role, label == null ? role.toString() : label);
        }
        return items;
    }

    public SelectItem[] getStatusSelectItems() {
        SelectItem[] items = new SelectItem[UserStatus.values().length];

        int i = 0;
        for (UserStatus status : UserStatus.values()) {
            items[i++] = new SelectItem(status, status.toString());
        }
        return items;
    }

    public SelectItem[] getPaysSelectItems() {
        SelectItem[] items = new SelectItem[UserPays.values().length];

        int i = 0;
        for (UserPays pays : UserPays.values()) {
            items[i++] = new SelectItem(pays, getLabelForPays(pays));
        }
        return items;
    }

    public String getLabelForPays(UserPays value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_user_pays_" + value);
        return label == null ? value.toString() : label;
    }

    public SelectItem[] getProvinceSelectItems() {
        SelectItem[] items = new SelectItem[UserProvince.values().length];

        int i = 0;
        for (UserProvince province : UserProvince.values()) {
            items[i++] = new SelectItem(province, getLabelForProvince(province));
        }
        return items;
    }

    public String getLabelForProvince(UserProvince value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_user_province_" + value);
        return label == null ? value.toString() : label;
    }

    public void loadCurrentUser() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            this.user = this.userService.findUserByUsername(username);
        } else {
            throw new RuntimeException("Can not get authorized user name");
        }
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public String creerCompte(int x) throws IOException {
        ArrayList<UserRole> roles = new ArrayList<>();
        roles.add(UserRole.Registered);
        user.setRoles(roles);
        user.setStatus(UserStatus.Active);
        persist();

        UserEntity user1 = userService.findUserByUsername(user.getUsername());

        String codeClient = user.getPays().name().substring(0, 2)
                + "-" + user.getProvince().name().substring(0, 2)
                + "-" + user.getId();
        user1.setCode(codeClient);
        this.setUser(user1);
        persist();

        CompteEntity compte = new CompteEntity();
        compte.setClient(user1);
        compte.setCategorie(getCategorie());
        if (x == 0) {
            compte.setStatus(CompteStatus.NONPAYE);
            compte.setType("CLIENT");
        }
        myService.addCompte(compte, user1);
        NoeudEntity noeudEntity = new NoeudEntity();
        noeudEntity.setCompteEnfant(compte);
        noeudService.save(noeudEntity);

        return "/pages/main.xhtml";
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }
}
