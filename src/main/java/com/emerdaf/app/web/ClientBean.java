/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emerdaf.app.web;

import com.emerdaf.app.domain.CompteEntity;
import com.emerdaf.app.domain.security.UserEntity;
import com.emerdaf.app.service.CompteService;
import com.emerdaf.app.service.security.SecurityWrapper;
import com.emerdaf.app.service.security.UserService;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author Norah
 */
@Named(value = "clientBean")
@SessionScoped
public class ClientBean implements Serializable {

    @Inject
    private UserService userService;
//

    private int[] nbreEnfParNiveau = new int[7];
    
    @Inject
    private CompteService compteService;
//
    private UserEntity client;
    private CompteEntity compte;
    private List<CompteEntity> comptes = new ArrayList<>();
//
//    private int[] nbreEnfParNiveau = new int[7];
//
//    private TreeNode root;
//    private TreeNode selectedNode;
//

    public ClientBean() {
    }

    @PostConstruct
    public void init() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            this.client = this.userService.findUserByUsername(username);
        } else {
            throw new RuntimeException("Can not get authorized user name");
        }

    }
//

    public UserEntity getClient() {
        if (client == null) {
            client = new UserEntity();
        }
        return client;
    }

    public void setClient(UserEntity client) {
        this.client = client;
    }

    public CompteEntity getCompte() {
        if (compte == null) {
            compte = new CompteEntity();
        }
        return compte;
    }

    public void setCompte(CompteEntity compte) {
        this.compte = compte;
    }

    public List<CompteEntity> getComptes() {
        return compteService.findComptesByClient(client);
    }

    public void setComptes(List<CompteEntity> comptes) {
        this.comptes = comptes;
    }
//
//    public List<CompteEntity> getEnfants() {
//        return compteService.getCmptEnfants(compte);
//    }
//
//    public void setEnfants(List<CompteEntity> enfants) {
//        this.enfants = enfants;
//    }
//
//    public CompteEntity getNoeudParent() {
//        return compteService.findNoeudCompteParentByCompteEnfant(compte);
//    }
//
//    public void setNoeudParent(CompteEntity noeudParent) {
//        this.noeudParent = noeudParent;
//    }
//
//    public TreeNode getSelectedNode() {
//        return selectedNode;
//    }
//
//    public void setSelectedNode(TreeNode selectedNode) {
//        this.selectedNode = selectedNode;
//    }
//
//    public int[] getNbreEnfParNiveau() {
//        return nbreEnfParNiveau;
//    }
//
//    public void setNbreEnfParNiveau(int[] nbreEnfParNiveau) {
//        this.nbreEnfParNiveau = nbreEnfParNiveau;
//    }
//
//    public String loadCmpt(CompteEntity c) {
//        this.setCompte(c);
//        root = new DefaultTreeNode(c, null);
////        for (int i = 0; i < 3; i++) {
////            nodes.add(new DefaultTreeNode(null, root));
////        }
//        return "/pages/compte/cmptView?faces-redirect=true";
//    }
//
//    public TreeNode getRoot() {
//        return root;
//    }
//
//    public void onNodeExpand(NodeExpandEvent event) {
//        CompteEntity c = (CompteEntity) event.getTreeNode().getData();
//        System.out.println("\n\nc2 = " + c.getCategorie());
////        List<CompteEntity> enfs = compteService.getCmptEnfants(compte);
////
//////        nodes.clear();
////        for (CompteEntity enf : enfs) {
////            System.out.println("\n\nenf = " + enf);
////            nodes.add(new DefaultTreeNode(enf, root));
////        }
//    }
//
//    public void onNodeSelect(NodeSelectEvent event) {
//        CompteEntity c = (CompteEntity) event.getTreeNode().getData();
//        System.out.println("\n\nc2 = " + c.getCategorie());
//        List<CompteEntity> enfs = compteService.getCmptEnfants(compte);
//
////        nodes.clear();
//        for (CompteEntity enf : enfs) {
//            nodes.add(new DefaultTreeNode(enf, root));
//        }
//    }
//
//    public List<CompteEntity> getEnfs(CompteEntity c) {
//        return compteService.getCmptEnfants(c);
//    }

    public int getNbreEnfs(CompteEntity c) {
        int x1, x2 = 0, x3 = 0, x4 = 0, x5 = 0, x6 = 0, x7 = 0;

        List<CompteEntity> l1 = compteService.getCmptEnfants(c);
        x1 = l1.size();
        for (CompteEntity n1 : l1) {
            List<CompteEntity> l2 = compteService.getCmptEnfants(n1);
            x2 = x2 + l2.size();
            for (CompteEntity n2 : l2) {
                List<CompteEntity> l3 = compteService.getCmptEnfants(n2);
                x3 = x3 + l3.size();
                for (CompteEntity n3 : l3) {
                    List<CompteEntity> l4 = compteService.getCmptEnfants(n3);
                    x4 = x4 + l4.size();
                    for (CompteEntity n4 : l4) {
                        List<CompteEntity> l5 = compteService.getCmptEnfants(n4);
                        x5 = x5 + l5.size();
                        for (CompteEntity n5 : l5) {
                            List<CompteEntity> l6 = compteService.getCmptEnfants(n5);
                            x6 = x6 + l6.size();
                            for (CompteEntity n6 : l6) {
                                List<CompteEntity> l7 = compteService.getCmptEnfants(n6);
                                x7 = x7 + l7.size();
                            }
                        }
                    }
                }
            }
        }
        System.out.println("=================");
        System.out.println("x1 = " + x1);
        System.out.println("x2 = " + x2);
        System.out.println("x3 = " + x3);
        System.out.println("x4 = " + x4);
        System.out.println("x5 = " + x5);
        System.out.println("x6 = " + x6);
        System.out.println("x7 = " + x7);
        System.out.println("==================");
        int[] tab = {x1, x2, x3, x4, x5, x6, x7};
        nbreEnfParNiveau = tab;
        return x1 + x2 + x3 + x4 + x5 + x6 + x7;
    }

    
//    public String creerCompte() throws IOException {
//
////        CompteEntity cmpt = new CompteEntity();
//        compte.setClientIndex(this.client);
//        compte.setStatus(CompteStatus.NonPaye);
//        compteService.add(compte);
//        CompteEntity noeud = new CompteEntity();
//        noeud.setCompteindex(compte);
//        compteService.save(noeud);
////        cmpt.setNoeudCompteIndex(noeud);
////        compteService.update2(cmpt);
////        loginBean.login(user.getUsername(), user.getPhone());
////        return null;
//        return "compte.xhtml?faces-redirect=true";
//    }

    public int[] getNbreEnfParNiveau() {
        return nbreEnfParNiveau;
    }

    public void setNbreEnfParNiveau(int[] nbreEnfParNiveau) {
        this.nbreEnfParNiveau = nbreEnfParNiveau;
    }
}
